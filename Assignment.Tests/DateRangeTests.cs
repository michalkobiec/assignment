﻿using System;
using NUnit.Framework;

namespace Assignment.Tests
{
    [TestFixture]
    public class DateRangeTests
    {
        [TestCaseSource(nameof(ToStringCases))]
        public void ToString_ReturnsCorrectRangeRepresentation_ForValidDates(DateTime fromDate, DateTime toDate, string expected)
        {
            var dateRange = new DateRange(fromDate, toDate);

            Assert.AreEqual(dateRange.ToString(), expected);
        }

        [Test]
        public void Creation_ThrowsException_WhenToDateIsBeforeFromDate()
        {
            var fromDate = new DateTime(2017, 1, 5);
            var invalidToDate = new DateTime(2016, 1, 5);

            Assert.Throws<ArgumentException>(() => new DateRange(fromDate, invalidToDate));
        }

        private static readonly object[] ToStringCases =
        {
            new object[] {new DateTime(2017, 1, 1), new DateTime(2017, 1, 5), "01 - 05.01.2017"},
            new object[] {new DateTime(2017, 1, 1), new DateTime(2017, 2, 5), "01.01 - 05.02.2017"},
            new object[] {new DateTime(2016, 1, 1), new DateTime(2017, 1, 5), "01.01.2016 - 05.01.2017" }
        };
    }
}