﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;

namespace Assignment.Tests
{
    [TestFixture]
    public class ArgsDateRangeProviderTests
    {
        private const string InvalidDateFormatMessageTemplate =
            "{0} date provided: {1} has invalid format. System date format: {2}, where d - day, M - month, y - year";
        private static readonly CultureInfo PolishCulture = CultureInfo.GetCultureInfo("pl-PL");
        private static readonly CultureInfo StatesCulture = CultureInfo.GetCultureInfo("en-US");
        private static readonly CultureInfo JapanCulture = CultureInfo.GetCultureInfo("ja-JP");
        private static CultureInfo _defaultCulture;
        
        private static IEnumerable<TestCaseData> InvalidDateStringsTestCaseData
        {
            get
            {
                yield return new TestCaseData(
                    new [] {"not date", "not date"},
                    new []
                    {
                        string.Format(InvalidDateFormatMessageTemplate, "from", "not date", PolishCulture.DateTimeFormat.ShortDatePattern),
                        string.Format(InvalidDateFormatMessageTemplate, "to", "not date", PolishCulture.DateTimeFormat.ShortDatePattern)
                    }, 
                    PolishCulture)
                    .SetName("Strings not representing dates, polish culture (dd.MM.yyyy)");

                yield return new TestCaseData(
                    new [] {"2008.12.31","30.04.2012"}, 
                    new[]
                    {
                        string.Format(InvalidDateFormatMessageTemplate, "to", "30.04.2012", JapanCulture.DateTimeFormat.ShortDatePattern),
                    },
                    JapanCulture)
                    .SetName("From date valid, to date invalid format, japan culture (yyyy.MM.dd)");

                yield return new TestCaseData(
                    new [] {"30.04.2012", "28.02.2013"}, 
                    new[]
                    {
                        string.Format(InvalidDateFormatMessageTemplate, "from", "30.04.2012", StatesCulture.DateTimeFormat.ShortDatePattern),
                        string.Format(InvalidDateFormatMessageTemplate, "to", "28.02.2013", StatesCulture.DateTimeFormat.ShortDatePattern)
                    }, 
                    StatesCulture)
                    .SetName("From date invalid format, to date invalid format, US culture (MM.dd.yyyy)");
            }
        }

        private static IEnumerable<TestCaseData> ValidDateStringsTestCaseData
        {
            get
            {
                yield return new TestCaseData(
                    new [] {"11.04.1993", "10.05.1993"},
                    new DateRange(new DateTime(1993, 4, 11), new DateTime(1993, 5, 10)),
                    PolishCulture)
                    .SetName("Polish culture (dd.MM.yyyy)");

                yield return new TestCaseData(
                    new [] {"2008.11.10","2008.12.09"}, 
                    new DateRange(new DateTime(2008, 11, 10), new DateTime(2008, 12, 9)),
                    JapanCulture)
                    .SetName("Japan culture (yyyy.MM.dd)");

                yield return new TestCaseData(
                    new [] {"04.04.2012", "05.03.2012"},
                    new DateRange(new DateTime(2012, 4, 4), new DateTime(2012, 5, 3)),
                    StatesCulture)
                    .SetName("US culture (MM.dd.yyyy)");
            }
        }

        [OneTimeSetUp]
        public void Setup()
        {
            _defaultCulture = CultureInfo.CurrentCulture;
        }

        [TearDown]
        public void TearDown()
        {
            CultureInfo.CurrentCulture = _defaultCulture;
        }

        [TestCase]
        [TestCase("11.04.1993")]
        public void TryGetDateRange_FailsAndHaveCorrectMessage_WhenArgumentNumberIsIncorrect(params string[] args)
        {
            var argsProvider = new ArgsDateRangeProvider(args);
            var expectedMessage = $"Invalid number of parameters - {args.Length} instead of 2";

            var result = argsProvider.TryGetDateRange(out _);
            var errorMessages = argsProvider.ReasonsCannotProvide;

            Assert.IsFalse(result);
            Assert.That(errorMessages, Has.Count.EqualTo(1));
            Assert.That(errorMessages.First(), Is.EqualTo(expectedMessage));
        }

        [Test, TestCaseSource(typeof(ArgsDateRangeProviderTests), nameof(InvalidDateStringsTestCaseData))]
        public void 
            TryGetDateRange_FailsAndHaveCorrectMessages_WhenDatesAreNotParsable_InSpecificCulture(string[] args, string[] errors, CultureInfo culture)
        {
            CultureInfo.CurrentCulture = culture;
            var argsProvider = new ArgsDateRangeProvider(args);

            var result = argsProvider.TryGetDateRange(out _);
            var errorMessages = argsProvider.ReasonsCannotProvide;

            Assert.IsFalse(result);
            CollectionAssert.AreEqual(errorMessages, errors);
        }

        [Test]
        public void TryGetDateRange_FailsAndHaveCorrectMessage_WhenToDateIsBeforeFromDate()
        {
            CultureInfo.CurrentCulture = PolishCulture;
            const string fromDate = "11.04.2018";
            const string invalidToDate = "10.04.2018";
            var argsProvider = new ArgsDateRangeProvider(new [] {fromDate, invalidToDate});
            var expectedErrorMessage =
                $"To date cannot be before From date in date range. System date format: {PolishCulture.DateTimeFormat.ShortDatePattern}, where d - day, M - month, y - year";

            var result = argsProvider.TryGetDateRange(out _);
            var errorMessages = argsProvider.ReasonsCannotProvide; Assert.IsFalse(result);

            Assert.IsFalse(result);
            Assert.That(errorMessages, Has.Count.EqualTo(1));
            Assert.That(errorMessages.First(), Is.EqualTo(expectedErrorMessage));
        }

        [Test, TestCaseSource(typeof(ArgsDateRangeProviderTests), nameof(ValidDateStringsTestCaseData))]
        public void TryGetDateRange_ReturnsDateRange_WhenArgsAreValid_InSpecificCulture(string[] args,
            DateRange expected, CultureInfo culture)
        {
            CultureInfo.CurrentCulture = culture;
            var argsProvider = new ArgsDateRangeProvider(args);

            var result = argsProvider.TryGetDateRange(out var actual);

            Assert.IsTrue(result);
            Assert.AreEqual(expected, actual);
        }
    }
}