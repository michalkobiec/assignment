﻿using System;
using System.Collections.Generic;
using Assignment.Interfaces;
using Moq;
using NUnit.Framework;

namespace Assignment.Tests
{
    public class BootstrapperTest
    {
        [Test]
        public void Start_WritesTextRepresentationOfDateRange_WhenTryGetDateRangeSuccessful()
        {
            var textHandlerMock = new Mock<ITextHandler>();
            var expectedDateRange = new DateRange(new DateTime(1993, 4, 11), new DateTime(2018, 09, 30));
            var dateRangeProviderMock = new Mock<IDateRangeProvider>();
            dateRangeProviderMock.Setup(rp => rp.TryGetDateRange(out expectedDateRange)).Returns(true);
            var bootstraper = new Bootstrapper(textHandlerMock.Object, dateRangeProviderMock.Object);

            bootstraper.Start();

            textHandlerMock.Verify(th => th.WriteText(It.Is<string>(s => s == expectedDateRange.ToString())), Times.Exactly(1));
        }

        [Test]
        public void Start_WritesErrors_WhenTryGetDateRangeUnsuccessful()
        {
            var errorReasons = new List<string> {"error1", "error2"}.AsReadOnly();
            var textHandlerMock = new Mock<ITextHandler>();
            var expectedMessage = $"Unable to create date range:\n-{errorReasons[0]}\n-{errorReasons[1]}";
            DateRange outDateRange;
            var dateRangeProviderMock = new Mock<IDateRangeProvider>();
            dateRangeProviderMock.Setup(rp => rp.TryGetDateRange(out outDateRange)).Returns(false);
            dateRangeProviderMock.Setup(rp => rp.ReasonsCannotProvide).Returns(errorReasons);
            var bootstraper = new Bootstrapper(textHandlerMock.Object, dateRangeProviderMock.Object);

            bootstraper.Start();

            textHandlerMock.Verify(th => th.WriteText(It.Is<string>(s => s == expectedMessage)), Times.Exactly(1));
        }

        [Test]
        public void Creation_ThrowsArgumentException_WhenTextHandlerIsNull()
        {
            Assert.Throws<ArgumentException>(() => new Bootstrapper(null, new Mock<IDateRangeProvider>().Object));
        }

        [Test]
        public void Creation_ThrowsArgumentException_WhenDateRangeProviderIsNull()
        {
            Assert.Throws<ArgumentException>(() => new Bootstrapper(new Mock<ITextHandler>().Object, null));
        }
    }
}