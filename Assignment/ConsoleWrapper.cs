﻿using System;
using Assignment.Interfaces;

namespace Assignment
{
    public class ConsoleWrapper: ITextHandler
    {
        public string ReadText() => Console.ReadLine();
        public void WriteText(string text) => Console.WriteLine(text);
    }
}