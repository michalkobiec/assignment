﻿using System;

namespace Assignment
{
    public struct DateRange
    {
        public DateTime From { get; }
        public DateTime To { get; }

        public DateRange(DateTime from, DateTime to)
        {
            if (from > to)
                throw new ArgumentException($"{nameof(from)} date have to be before {nameof(to)} date");

            From = from;
            To = to;
        }

        public override string ToString()
        {
            const string toFormat = "dd.MM.yyyy";
            string fromFormat;

            if (From.Year < To.Year)
                fromFormat = toFormat;
            else if (From.Month < To.Month)
                fromFormat = "dd.MM";
            else
                fromFormat = "dd";

            return $"{From.ToString(fromFormat)} - {To.ToString(toFormat)}";
        }

    }
}