namespace Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            var dateRangeProvider = new ArgsDateRangeProvider(args);
            var textHandler = new ConsoleWrapper();
            new Bootstrapper(textHandler, dateRangeProvider).Start();
        }
    }
}
