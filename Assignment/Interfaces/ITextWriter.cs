﻿namespace Assignment.Interfaces
{
    public interface ITextWriter
    {
        void WriteText(string text);
    }
}