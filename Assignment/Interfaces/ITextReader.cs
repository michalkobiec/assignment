﻿namespace Assignment.Interfaces
{
    public interface ITextReader
    {
        string ReadText();
    }
}