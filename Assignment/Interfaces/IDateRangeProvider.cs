﻿using System.Collections.Generic;

namespace Assignment.Interfaces
{
    public interface IDateRangeProvider
    {
        bool TryGetDateRange(out DateRange dateRange);
        IReadOnlyCollection<string> ReasonsCannotProvide { get; }
    }
}