﻿using System;
using Assignment.Interfaces;

namespace Assignment
{
    public class Bootstrapper
    {
        private readonly ITextHandler _textHandler;
        private readonly IDateRangeProvider _dateRangeProvider;

        public Bootstrapper(ITextHandler textHandler, IDateRangeProvider dateRangeProvider)
        {
            _textHandler = textHandler ?? throw new ArgumentException($"{nameof(textHandler)} cannot be null");
            _dateRangeProvider = dateRangeProvider ?? throw new ArgumentException($"{nameof(dateRangeProvider)} cannot be null");
        }

        public void Start()
        {
            if (_dateRangeProvider.TryGetDateRange(out var dateRange))
                _textHandler.WriteText(dateRange.ToString());
            else
                _textHandler.WriteText(GetErrorMessage());
            _textHandler.ReadText();
        }

        private string GetErrorMessage()
        {
            var errors = string.Join("\n-", _dateRangeProvider.ReasonsCannotProvide);
            return $"Unable to create date range:\n-{errors}";
        }
    }
}