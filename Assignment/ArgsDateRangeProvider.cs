﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Assignment.Interfaces;

namespace Assignment
{
    public class ArgsDateRangeProvider: IDateRangeProvider
    {
        private const int ExpectedNumberOfParameters = 2;
        public IReadOnlyCollection<string> ReasonsCannotProvide => _reasonsCannotProvide.AsReadOnly();
        private readonly List<string> _reasonsCannotProvide = new List<string>();
        private bool IsValid => !_reasonsCannotProvide.Any();
        private DateRange _dateRange;

        public ArgsDateRangeProvider(string[] args)
        {
            CreateDateRange(args);
        }

        public bool TryGetDateRange(out DateRange dateRange)
        {
            dateRange = _dateRange;
            return IsValid;
        }

        private void CreateDateRange(string[] args)
        {
            if (args.Length < ExpectedNumberOfParameters)
            {
                _reasonsCannotProvide
                    .Add($"Invalid number of parameters - {args.Length} instead of {ExpectedNumberOfParameters}");
                return;
            }

            var from = ParseDateTime(args[0], "from");
            var to = ParseDateTime(args[1], "to");
            if (from > to)
            {
                _reasonsCannotProvide.Add($"To date cannot be before From date in date range. {CurrentDateTimeFormatInfo}");
            }
            if (IsValid)
                _dateRange = new DateRange(from.Value, to.Value);
        }

        private DateTime? ParseDateTime(string stringDateTime, string paramName)
        {
            if (DateTime.TryParse(stringDateTime, out var dateTime))
                return dateTime;

            _reasonsCannotProvide
                .Add(
                    $"{paramName} date provided: {stringDateTime} has invalid format. {CurrentDateTimeFormatInfo}");
            return null;
        }

        private static string CurrentDateTimeFormatInfo =>
            $"System date format: {CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern}, where d - day, M - month, y - year";
    }
}